﻿using Assets.Scripts.Application.HTTP;
using MongoDB.Bson;
using PasswordSafeServer.Application.DTO;
using PasswordSafeServer.Domain.Models;
using System.Collections;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TableScene : MonoBehaviour
{
    public GameObject RowPrefab;
    public GameObject AddRow;
    public GameObject ErrPanel;

    public GameObject PasswordChangePanel;
    public InputField NewPassword;
    public InputField RepeatPassword;
    public Button ChangePassword;

    public GameObject EmailChangePanel;
    public InputField NewEmail;
    public Button ChangeEmail;

    private ClientController _ClientController;
    void Start()
    {
        _ClientController = new ClientController();
        GetData();
        UpdateTable();
    }

    void Update()
    {
        CheckJWTEcpiration();
    }

    public void OnCloseButtonPressed() => ErrPanel.SetActive(false);

    public void OnQuitPressed()
    {
        DeleteUser();
        SceneManager.LoadScene(0);
    }

    public void OnRefreshPressed()
    {
        GetData();
        UpdateTable();
    }

    public void OnDeleteAllPressed() => DeleteAll();
    public void OnClosePasswordPanelPressed() => PasswordChangePanel.SetActive(false);
    public void OnChangePasswordPanelPressed() => PasswordChangePanel.SetActive(true);
    public void OnCloseEmailPanelPressed() => EmailChangePanel.SetActive(false);
    public void OnChangeEmailPanelPressed() => EmailChangePanel.SetActive(true);

    public void OnDeleteUserPressed()
    {
        var res = _ClientController.DeleteUser();

        if (!ClientController.IsSuccessStatusCode(res.Status))
        {
            ShowErrorMsg("Failed to delete user");
            return;
        }

        DeleteUser();
        SceneManager.LoadScene(0);
    }

    public void OnAddDataClick()
    {
        var data = new DataDTO()
        {
            Email = AddRow.transform.GetChild(1).GetComponent<InputField>().text,
            Login = AddRow.transform.GetChild(2).GetComponent<InputField>().text,
            Password = AddRow.transform.GetChild(3).GetComponent<InputField>().text,
            WebsiteOrDevice = AddRow.transform.GetChild(0).GetComponent<InputField>().text,
            ID = new ObjectId().ToString()
        };

        var res = _ClientController.AddData(data);

        if (!ClientController.IsSuccessStatusCode(res.Status))
        {
            ShowErrorMsg("Failed data adding");
            return;
        }

        UpdateTable();
    }

    public void OnChangePasswordClick()
    {
        string pass1 = NewPassword.text,
            pass2 = RepeatPassword.text;
        if (!IsPasswordValid(pass1))
        {
            string err = "Password must contain at least:\n" +
                "1) One digit\n" +
                "2) One letter in low case\n" +
                "3) One letter in high case\n" +
                "4) Six symbols\n" +
                "5) One special character (!@#$%^&*)\n" +
                "6) Password must contain only letters, digits and special characters";
            ShowErrorMsg(err);
            return;
        }

        if (pass1 != pass2)
        {
            string err = "Passwords mismatch";
            ShowErrorMsg(err);
            return;
        }

        var res = _ClientController.ChangePassword(new AuthDTO() { Password = pass1 });
        if (!ClientController.IsSuccessStatusCode(res.Status))
        {
            ShowErrorMsg("Unsuccessful password change");
            return;
        }

        PasswordChangePanel.SetActive(false);
    }

    public void OnChangeEmailClick()
    {
        string email = NewEmail.text;
        if (!IsEmailValid(email))
        {
            ShowErrorMsg("Invalid email");
            return;
        }

        var res = _ClientController.ChangeEmail(new AuthDTO() { Email = email });
        if (!ClientController.IsSuccessStatusCode(res.Status))
        {
            ShowErrorMsg("Unsuccessful email change");
            return;
        }

        DeleteUser();
        SceneManager.LoadScene(0);
    }

    private void GetData()
    {
        if (!ClientController.IsSuccessStatusCode(_ClientController.GetAllData().Status))
        {
            ShowErrorMsg("Failed data loading");
            return;
        }
    }

    private void UpdateTable()
    {
        var parent = RowPrefab.transform.parent;
        if (parent.childCount > 1)
        {
            var itemsToRemove = new List<GameObject>(parent.childCount - 1);
            for (int i = 0; i < parent.childCount; i++)
            {
                var childGO = parent.GetChild(i).gameObject;
                if (childGO.activeSelf)
                    itemsToRemove.Add(childGO);
            }
            for (int i = 0; i < itemsToRemove.Count; i++)
                Destroy(itemsToRemove[i]);
        }

        var elements = ClientController.User.DataList.DataModels;

        foreach (var el in elements)
        {
            var tempRow = Instantiate(RowPrefab);
            tempRow.SetActive(true);
            tempRow.transform.parent = RowPrefab.transform.parent.transform;
            tempRow.transform.GetChild(0).GetComponent<InputField>().text = el.WebsiteOrDevice;
            tempRow.transform.GetChild(0).GetComponent<InputField>().onEndEdit.AddListener(delegate
            {
                el.WebsiteOrDevice = tempRow.transform.GetChild(0).GetComponent<InputField>().text;
                EditData(el);
            });

            tempRow.transform.GetChild(1).GetComponent<InputField>().text = el.Email;
            tempRow.transform.GetChild(1).GetComponent<InputField>().onEndEdit.AddListener(delegate
            {
                el.Email = tempRow.transform.GetChild(1).GetComponent<InputField>().text;
                EditData(el);
            });

            tempRow.transform.GetChild(2).GetComponent<InputField>().text = el.Login;
            tempRow.transform.GetChild(2).GetComponent<InputField>().onEndEdit.AddListener(delegate
            {
                el.Login = tempRow.transform.GetChild(2).GetComponent<InputField>().text;
                EditData(el);
            });

            tempRow.transform.GetChild(3).GetComponent<InputField>().text = el.Password;
            tempRow.transform.GetChild(3).GetComponent<InputField>().onEndEdit.AddListener(delegate
            {
                el.Password = tempRow.transform.GetChild(3).GetComponent<InputField>().text;
                EditData(el);
            });

            tempRow.transform.GetChild(4).GetComponent<Button>().onClick.AddListener(delegate
            {
                DeleteDataElement(el, tempRow); 
            });
        }
    }

    private void ShowErrorMsg(string msg)
    {
        ErrPanel.GetComponentInChildren<Text>().text = msg;
        ErrPanel.SetActive(true);
    }

    private DataDTO ConvertFromDataModel(DataModel data)
        => new DataDTO()
        {
            ID = data.ID.ToString(),
            Email = data.Email,
            Login = data.Login,
            Password = data.Password,
            WebsiteOrDevice = data.WebsiteOrDevice
        };

    private void EditData(DataModel data)
    {
        var res = _ClientController.ChangeData(ConvertFromDataModel(data));
        if (!ClientController.IsSuccessStatusCode(res.Status))
        {
            ShowErrorMsg(res.Status.ToString());
            return;
        }
        UpdateTable();
    }

    private void DeleteDataElement(DataModel data, GameObject obj)
    {
        var res = _ClientController.DeleteDataElement(ConvertFromDataModel(data));
        if (!ClientController.IsSuccessStatusCode(res.Status))
        {
            ShowErrorMsg(res.Status.ToString());
            return;
        }

        GameObject.Destroy(obj);
    }

    private void DeleteAll()
    {
        var res = _ClientController.DeleteAllData();
        if (!ClientController.IsSuccessStatusCode(res.Status))
        {
            ShowErrorMsg(res.Status.ToString());
            return;
        }
        UpdateTable();
    }

    private bool IsPasswordValid(string password)
    {
        if (string.IsNullOrEmpty(password))
            return false;
        // Password must contain at least one digit
        // Password must contain at least one letter in low case
        // Password must contain at least one letter in high case
        // Password must contain at least six symbols
        // Password must contain at least one special character (!@#$%^&*)           
        Regex rule1 = new Regex("(?=.*[0-9])(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{6,}");
        // Password must contain only letters, digits and special characters
        Regex rule2 = new Regex("[^0-9a-zA-Z!@#$%^&*]");

        return rule1.IsMatch(password) && !rule2.IsMatch(password);
    }

    private bool IsEmailValid(string email)
    {
        try
        {
            new MailAddress(email);
            return true;
        }
        catch
        {
            return false;
        }
    }

    private void DeleteUser()
    {
        ClientController.User.JWT = string.Empty;
        ClientController.User.RefreshToken = string.Empty;
        ClientController.User.Email = string.Empty;
        ClientController.User.DataList = new DataListModel();

    }

    private void CheckJWTEcpiration()
    {
        if (ClientController.IsJWTNotExpired(ClientController.User.JWT))
            return;

        if (!ClientController.IsJWTNotExpired(ClientController.User.RefreshToken))
            SceneManager.LoadScene(0);
        else
        {
            var res = _ClientController.RefreshTokens();
            if (!ClientController.IsSuccessStatusCode(res.Status))
                SceneManager.LoadScene(0);
        }
    }
}
