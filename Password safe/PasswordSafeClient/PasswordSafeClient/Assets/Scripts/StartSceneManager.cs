﻿using Assets.Plugins.JWT;
using Assets.Scripts.Application.HTTP;
using PasswordSafeClient.Domain.Models;
using PasswordSafeServer.Application.DTO;
using System;
using System.Net;
using System.Net.Mail;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StartSceneManager : MonoBehaviour
{
    public InputField Email;
    public InputField Password;
    public InputField RepeatPassword;
    public GameObject ErrPanel;
    public Button LoginRegister;
    public Button ChangeToLoginOrRegister;

    private ClientController _ClientController;
    private bool isOnRegistration;
    private bool IsOnRegistration
    {
        set
        {
            isOnRegistration = value;
            OnLoginRegistationChange();
        }
        get
        {
            return isOnRegistration;
        }
    }

    void Start()
    {
        IsOnRegistration = false;
        _ClientController = new ClientController();

        Email.text = "artur@gmail.com";
        Password.text = "!Artur4715982";
        
    }

    public void OnCloseButtonPressed() => ErrPanel.SetActive(false);

    public void OnChangeToButtonPressed() => IsOnRegistration = !IsOnRegistration;

    public void OnLoginOrRegisterPressed()
    {
        ReturnModel<TokensDTO> res;
        var auth = new AuthDTO()
        {
            Email = Email.text,
            Password = Password.text
        };

        if (HasAuthDataErrors(auth))
            return;

        if (IsOnRegistration)
        {
            if (RepeatPassword.text != auth.Password)
            {
                ShowErrorMsg("Passwords mismatch");
                return;
            }
            res = _ClientController.Registration(auth);
        }
        else
            res = _ClientController.Login(auth);

        if (!ClientController.IsSuccessStatusCode(res.Status))
        {
            ShowErrorMsg(res.Status.ToString());
            return;
        }

        if (!string.IsNullOrEmpty(ClientController.User?.JWT) && 
            JwtUtils.ParseJwt(ClientController.User.JWT).Body.Expires > DateTime.UtcNow)
            SceneManager.LoadScene(1);
        else
        {
            ShowErrorMsg("Client error");
        }
    }

    private void OnLoginRegistationChange()
    {
        if (IsOnRegistration)
        {
            RepeatPassword.gameObject.SetActive(true);
            LoginRegister.gameObject.GetComponentInChildren<Text>().text = "Register";
            ChangeToLoginOrRegister.gameObject.GetComponentInChildren<Text>().text = "Change to login";
        }
        else
        {
            RepeatPassword.gameObject.SetActive(false);
            LoginRegister.gameObject.GetComponentInChildren<Text>().text = "Login";
            ChangeToLoginOrRegister.gameObject.GetComponentInChildren<Text>().text = "Change to registration";
        }
    }

    private void ShowErrorMsg(string msg)
    {
        ErrPanel.GetComponentInChildren<Text>().text = msg;
        ErrPanel.SetActive(true);
    }

    private static bool IsEmailValid(string email)
    {
        try
        {
            new MailAddress(email);
            return true;
        }
        catch
        {
            return false;
        }
    }

    private static bool IsPasswordValid(string password)
    {
        if (string.IsNullOrEmpty(password))
            return false;
        // Password must contain at least one digit
        // Password must contain at least one letter in low case
        // Password must contain at least one letter in high case
        // Password must contain at least six symbols
        // Password must contain at least one special character (!@#$%^&*)           
        Regex rule1 = new Regex("(?=.*[0-9])(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{6,}");
        // Password must contain only letters, digits and special characters
        Regex rule2 = new Regex("[^0-9a-zA-Z!@#$%^&*]");

        return rule1.IsMatch(password) && !rule2.IsMatch(password);
    }

    private bool HasAuthDataErrors(AuthDTO auth)
    {
        if (!IsEmailValid(auth.Email))
        {
            ShowErrorMsg("Invalid email");
            return true;
        }

        if (!IsPasswordValid(auth.Password))
        {
            string msg =
                "Password must contain at least:\n" +
                "1) One digit\n" +
                "2) One letter in low case\n" +
                "3) One letter in high case\n" +
                "4) Six symbols\n" +
                "5) One special character (!@#$%^&*)\n" +
                "6) Password must contain only letters, digits and special characters";
            ShowErrorMsg(msg);
            return true;
        }

        return false;
    }
}
