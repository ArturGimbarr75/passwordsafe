﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;

namespace PasswordSafeServer.Application.DTO
{
    public class DataListDTO
    {
        public List<DataDTO> DataModels { get; set; }
    }

    public class DataDTO
    {
        public string ID { get; set; }
        public string WebsiteOrDevice { get; set; }
        public string Email { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }

    }
}
