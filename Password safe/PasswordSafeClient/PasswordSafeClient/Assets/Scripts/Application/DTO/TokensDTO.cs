﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PasswordSafeServer.Application.DTO
{
    public class TokensDTO
    {
        public string JWT { get; set; }
        public string RefreshToken { get; set; }
    }
}
