﻿using PasswordSafeClient.Domain.Models;
using PasswordSafeServer.Application.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Application.HTTP
{
    interface IClientController
    {
        #region User
        ReturnModel<TokensDTO> Registration(AuthDTO auth);
        ReturnModel<TokensDTO> Login(AuthDTO auth);
        ReturnModel DeleteUser();
        ReturnModel ChangePassword(AuthDTO auth);
        ReturnModel ChangeEmail(AuthDTO auth);
        #endregion

        #region Data
        ReturnModel AddData(DataDTO data);
        ReturnModel ChangeData(DataDTO data);
        ReturnModel DeleteDataElement(DataDTO data);
        ReturnModel DeleteAllData();
        ReturnModel<DataListDTO> GetAllData();
        #endregion
    }
}
