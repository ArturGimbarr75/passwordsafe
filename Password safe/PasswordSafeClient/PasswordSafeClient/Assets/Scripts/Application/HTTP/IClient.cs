﻿using PasswordSafeServer.Application.DTO;
using PasswordSafeClient.Domain.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace PasswordSafeClient.Application.HTTP
{
    interface IClient
    {
        #region User
        ReturnModel<TokensDTO> Registration(AuthDTO auth);
        ReturnModel<TokensDTO> Login(AuthDTO auth);
        ReturnModel<TokensDTO> Refresh(string refreshToken);
        ReturnModel DeleteUser(string token);
        ReturnModel ChangePassword(AuthDTO auth, string token);
        ReturnModel ChangeEmail(AuthDTO auth, string token);
        #endregion

        #region Data
        ReturnModel AddData(DataDTO data, string token);
        ReturnModel ChangeData(DataDTO data, string token);
        ReturnModel DeleteDataElement(DataDTO data, string token);
        ReturnModel DeleteAllData(string token);
        ReturnModel<DataListDTO> GetAllData(string token);
        #endregion
    }
}
