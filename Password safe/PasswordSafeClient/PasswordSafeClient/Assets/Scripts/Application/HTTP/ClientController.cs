﻿using Assets.Scripts.Application.JWT;
using MongoDB.Bson;
using PasswordSafeClient.Application.HTTP;
using PasswordSafeClient.Domain.Models;
using PasswordSafeServer.Application.DTO;
using PasswordSafeServer.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Application.HTTP
{
    class ClientController : IClientController
    {
        private Client _Client;
        public static UserModel User { get; private set; }

        public ClientController()
        {
            _Client = new Client();
            if (User == null)
                User = new UserModel();
        }

        public ReturnModel<TokensDTO> Registration(AuthDTO auth)
        {
            try
            {
                var res =_Client.Registration(auth);
                
                if (!IsSuccessStatusCode(res.Status))
                    return res;

                User.JWT = res.Value.JWT;
                User.RefreshToken = res.Value.RefreshToken;
                User.Email = auth.Email;
                return res;
            }
            catch (Exception ex)
            {
                Debug.LogException(ex);
                return new ReturnModel<TokensDTO>() { Status = HttpStatusCode.BadRequest };
            }
        }

        public ReturnModel<TokensDTO> Login(AuthDTO auth)
        {
            try
            {
                var res = _Client.Login(auth);

                if (!IsSuccessStatusCode(res.Status))
                    return res;

                User.JWT = res.Value.JWT;
                User.RefreshToken = res.Value.RefreshToken;
                User.Email = auth.Email;
                return res;
            }
            catch (Exception ex)
            {
                Debug.LogException(ex);
                return new ReturnModel<TokensDTO>() { Status = HttpStatusCode.BadRequest };
            }
        }

        public ReturnModel<TokensDTO> RefreshTokens()
        {
            try
            {
                var res = _Client.Refresh(User.RefreshToken);

                if (!IsSuccessStatusCode(res.Status))
                    return res;

                User.JWT = res.Value.JWT;
                User.RefreshToken = res.Value.RefreshToken;
                return res;
            }
            catch (Exception ex)
            {
                Debug.LogException(ex);
                return new ReturnModel<TokensDTO>() { Status = HttpStatusCode.BadRequest };
            }
        }

        public ReturnModel DeleteUser()
        {
            try
            {
                var res = _Client.DeleteUser(User.JWT);

                if (!IsSuccessStatusCode(res.Status))
                    return res;

                User = new UserModel();
                return res;
            }
            catch (Exception ex)
            {
                Debug.LogException(ex);
                return new ReturnModel<TokensDTO>() { Status = HttpStatusCode.BadRequest };
            }
        }

        public ReturnModel ChangePassword(AuthDTO auth)
        {
            try
            {
                var res = _Client.ChangePassword(auth, User.JWT);
                return res;
            }
            catch (Exception ex)
            {
                Debug.LogException(ex);
                return new ReturnModel<TokensDTO>() { Status = HttpStatusCode.BadRequest };
            }
        }

        public ReturnModel ChangeEmail(AuthDTO auth)
        {
            try
            {
                var res = _Client.ChangeEmail(auth, User.JWT);

                if (!IsSuccessStatusCode(res.Status))
                    return res;

                User.Email = auth.Email ;
                return res;
            }
            catch (Exception ex)
            {
                Debug.LogException(ex);
                return new ReturnModel<TokensDTO>() { Status = HttpStatusCode.BadRequest };
            }
        }

        public ReturnModel AddData(DataDTO data)
        {
            try
            {
                var res = _Client.AddData(data, User.JWT);

                if (!IsSuccessStatusCode(res.Status))
                    return res;

                User.DataList.DataModels.Add(ConvertDataDTOToDataModel(data));
                return res;
            }
            catch (Exception ex)
            {
                Debug.LogException(ex);
                return new ReturnModel<TokensDTO>() { Status = HttpStatusCode.BadRequest };
            }
        }

        public ReturnModel ChangeData(DataDTO data)
        {
            try
            {
                var res = _Client.ChangeData(data, User.JWT);

                if (!IsSuccessStatusCode(res.Status))
                    return res;

                User.DataList.DataModels.RemoveAll(x => x.ID.ToString() == data.ID);
                User.DataList.DataModels.Add(ConvertDataDTOToDataModel(data));
                return res;
            }
            catch (Exception ex)
            {
                Debug.LogException(ex);
                return new ReturnModel<TokensDTO>() { Status = HttpStatusCode.BadRequest };
            }
        }

        public ReturnModel DeleteDataElement(DataDTO data)
        {
            try
            {
                var res = _Client.DeleteDataElement(data, User.JWT);

                if (!IsSuccessStatusCode(res.Status))
                    return res;

                User.DataList.DataModels.RemoveAll(x => x.ID.ToString() == data.ID);
                return res;
            }
            catch (Exception ex)
            {
                Debug.LogException(ex);
                return new ReturnModel<TokensDTO>() { Status = HttpStatusCode.BadRequest };
            }
        }

        public ReturnModel DeleteAllData()
        {
            try
            {
                var res = _Client.DeleteAllData(User.JWT);

                if (!IsSuccessStatusCode(res.Status))
                    return res;

                User.DataList.DataModels.Clear();
                return res;
            }
            catch (Exception ex)
            {
                Debug.LogException(ex);
                return new ReturnModel<TokensDTO>() { Status = HttpStatusCode.BadRequest };
            }
        }

        public ReturnModel<DataListDTO> GetAllData()
        {
            try
            {
                var res = _Client.GetAllData(User.JWT);

                if (!IsSuccessStatusCode(res.Status))
                    return res;

                User.DataList.DataModels = res.Value.DataModels.Select(x => ConvertDataDTOToDataModel(x)).ToList();
                return res;
            }
            catch (Exception ex)
            {
                Debug.LogException(ex);
                return new ReturnModel<DataListDTO>() { Status = HttpStatusCode.BadRequest };
            }
        }

        public static bool IsSuccessStatusCode(HttpStatusCode statusCode)
            =>((int)statusCode >= 200) && ((int)statusCode <= 299);

        public static bool IsJWTNotExpired(string jwt)
        {
            JwtModel model = JwtUtils.ParseJwt(jwt);
            return model.Body?.Expires != null && model.Body.Expires > DateTime.UtcNow;
        }

        private DataModel ConvertDataDTOToDataModel(DataDTO data)
            => new DataModel() { ID = new ObjectId(data.ID), Email = data.Email, Login = data.Login, Password = data.Password, WebsiteOrDevice = data.WebsiteOrDevice };

    }
}
