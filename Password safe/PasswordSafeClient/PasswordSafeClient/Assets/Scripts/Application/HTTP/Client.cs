﻿//using CI.HttpClient;
using Newtonsoft.Json;
using PasswordSafeClient.Domain.Models;
using PasswordSafeServer.Application.DTO;
using PasswordSafeServer.Domain.Models;
using System;
/**/
using System.Net.Http;
using System.Net.Http.Headers;
/**/
using System.Text;

namespace PasswordSafeClient.Application.HTTP
{
    class Client : IClient
    {
        private static HttpClient HTTPClient;
        private const string BaseUrl = "http://192.168.1.103:5000/";

        public Client()
        {
            if (HTTPClient == null)
                HTTPClient = new HttpClient() { BaseAddress = new Uri(BaseUrl)};
        }

        public ReturnModel AddData(DataDTO data, string token)
        {
            const string url = "api/data/add";
            HTTPClient.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", token);
            HttpContent content =
                new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json");
            HttpResponseMessage response = HTTPClient.PostAsync(url, content).Result;
            return new ReturnModel() { Status = response.StatusCode };
        }

        public ReturnModel ChangeData(DataDTO data, string token)
        {
            const string url = "api/data/change_data";
            HTTPClient.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", token);
            HttpContent content =
                new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json");
            HttpResponseMessage response = HTTPClient.PutAsync(url, content).Result;
            return new ReturnModel() { Status = response.StatusCode };
        }

        public ReturnModel ChangeEmail(AuthDTO auth, string token)
        {
            const string url = "api/auth/change_email";
            HTTPClient.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", token);
            HttpContent content =
                new StringContent(JsonConvert.SerializeObject(auth), Encoding.UTF8, "application/json");
            HttpResponseMessage response = HTTPClient.PutAsync(url, content).Result;
            return new ReturnModel() { Status = response.StatusCode };
        }

        public ReturnModel ChangePassword(AuthDTO auth, string token)
        {
            const string url = "api/auth/change_password";
            HTTPClient.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", token);
            HttpContent content =
                new StringContent(JsonConvert.SerializeObject(auth), Encoding.UTF8, "application/json");
            HttpResponseMessage response = HTTPClient.PutAsync(url, content).Result;
            return new ReturnModel() { Status = response.StatusCode };
        }

        public ReturnModel DeleteUser(string token)
        {
            const string url = "api/auth/delete";
            HTTPClient.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", token);
            HttpResponseMessage response = HTTPClient.DeleteAsync(url).Result;
            return new ReturnModel() { Status = response.StatusCode };
        }

        public ReturnModel DeleteAllData(string token)
        {
            const string url = "api/data/delete_all_data";
            HTTPClient.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", token);
            HttpResponseMessage response = HTTPClient.DeleteAsync(url).Result;
            return new ReturnModel() { Status = response.StatusCode };
        }

        public ReturnModel DeleteDataElement(DataDTO data, string token)
        {
            const string url = "api/data/delete_element";
            HTTPClient.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", token);
            HttpContent content =
                new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json");
            HttpResponseMessage response = HTTPClient.PutAsync(url, content).Result;
            return new ReturnModel() { Status = response.StatusCode };
        }

        public ReturnModel<DataListDTO> GetAllData(string token)
        {
            const string url = "api/data/get_all_data";
            HTTPClient.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", token);
            HttpResponseMessage response = HTTPClient.GetAsync(url).Result;
            var resuslt = response.Content.ReadAsStringAsync().Result;

            var data = JsonConvert.DeserializeObject<DataListDTO>(resuslt);
            return new ReturnModel<DataListDTO>() { Status = response.StatusCode, Value = data };
        }

        public ReturnModel<TokensDTO> Login(AuthDTO auth)
        {
            const string url = "api/auth/login";
            HttpContent content =
                new StringContent(JsonConvert.SerializeObject(auth), Encoding.UTF8, "application/json");
            HttpResponseMessage response = HTTPClient.PostAsync(url, content).Result;
            var resuslt = response.Content.ReadAsStringAsync().Result;
            var data = JsonConvert.DeserializeObject<TokensDTO>(resuslt);

            return new ReturnModel<TokensDTO>() { Status = response.StatusCode, Value = data };
        }

        public ReturnModel<TokensDTO> Registration(AuthDTO auth)
        {
            const string url = "api/auth/register";
            HttpContent content =
                new StringContent(JsonConvert.SerializeObject(auth), Encoding.UTF8, "application/json");
            HttpResponseMessage response = HTTPClient.PostAsync(url, content).Result;
            var resuslt = response.Content.ReadAsStringAsync().Result;
            var data = JsonConvert.DeserializeObject<TokensDTO>(resuslt);
            return new ReturnModel<TokensDTO>() { Status = response.StatusCode, Value = data };
        }

        public ReturnModel<TokensDTO> Refresh(string refreshToken)
        {
            const string url = "api/auth/refresh";
            HttpContent content =
                new StringContent(JsonConvert.SerializeObject(new TokensDTO() { RefreshToken = refreshToken }), Encoding.UTF8, "application/json");
            HTTPClient.DefaultRequestHeaders.Authorization =
               new AuthenticationHeaderValue("Bearer", refreshToken);
            HttpResponseMessage response = HTTPClient.PostAsync(url, content).Result;
            var resuslt = response.Content.ReadAsStringAsync().Result;
            var data = JsonConvert.DeserializeObject<TokensDTO>(resuslt);
            return new ReturnModel<TokensDTO>() { Status = response.StatusCode, Value = data };
        }
    }
}
