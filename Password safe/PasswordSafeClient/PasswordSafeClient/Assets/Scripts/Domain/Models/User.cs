﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace PasswordSafeServer.Domain.Models
{
    public class UserModel
    {
        public UserModel() { DataList = new DataListModel();
            JWT = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9." +
                "eyJuYW1laWQiOiI1ZjIzYzc5M2Q2OTkyNjUwZTQzYzNmNTMiLCJlbWFpbCI6ImFydHVyQGdtYWlsLmNv" +
                "bSIsIm5iZiI6MTU5NjQ0NjYxMywiZXhwIjoxNTk5MTI1MDEzLCJpYXQiOjE1OTY0NDY2MTN9." +
                "9U3PpEYKhB8lkphKT8ku5HoyCYX6yQpvtFIC6dYpyMU"; // One year jwt
        } 
        public ObjectId _id { get; set; }
        public string Email { get; set; }
        public string JWT { get; set; }
        public string RefreshToken { get; set; }
        public DataListModel DataList { get; set; }
    }
}
