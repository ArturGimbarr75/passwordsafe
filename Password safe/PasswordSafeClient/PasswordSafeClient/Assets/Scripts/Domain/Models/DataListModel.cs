﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;

namespace PasswordSafeServer.Domain.Models
{
    public class DataListModel
    {
        public DataListModel()
            => DataModels = new List<DataModel>();
        public List<DataModel> DataModels { get; set; }
    }

    public class DataModel
    {
        public ObjectId ID { get; set; }
        public string WebsiteOrDevice { get; set; }
        public string Email { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
