﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace PasswordSafeClient.Domain.Models
{
    public class ReturnModel
    {
        public HttpStatusCode Status { get; set; }
    }

    public class ReturnModel<T> : ReturnModel
    {
        public T Value { get; set; }
    }
}
