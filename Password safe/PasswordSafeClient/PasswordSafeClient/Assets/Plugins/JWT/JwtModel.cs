﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Application.JWT
{
    public class JwtModel
    {
        public JwtHeader Header { get; set; }
        public JwtBody Body { get; set; }

        public string Token { get; set; }
    }

    public class JwtHeader
    {
        [JsonProperty(PropertyName = "alg")]
        public string Algorithm { get; set; }

        [JsonProperty(PropertyName = "typ")]
        public string Type { get; set; }
    }

    public class JwtBody
    {
        [JsonProperty(PropertyName = "nbf")]
        [JsonConverter(typeof(SecondsTimestampConverter))]
        public DateTime? NotBefore { get; set; }

        [JsonProperty(PropertyName = "exp")]
        [JsonConverter(typeof(SecondsTimestampConverter))]
        public DateTime? Expires { get; set; }

        [JsonProperty(PropertyName = "iat")]
        [JsonConverter(typeof(SecondsTimestampConverter))]
        public DateTime? IssuedAt { get; set; }

        [JsonProperty(PropertyName = "email")]
        public string Email { get; set; }
    }
    public static class JwtUtils
    {
        public static JwtModel ParseJwt(string token)
        {
            var splitInfo = token.Split('.');

            var headerStr = splitInfo[0].PadBase64StringToLength().DecodeBase64();

            var bodyStr = splitInfo[1].PadBase64StringToLength().DecodeBase64();

            var header = JsonConvert.DeserializeObject<JwtHeader>(headerStr);

            var body = JsonConvert.DeserializeObject<JwtBody>(bodyStr);

            return new JwtModel
            {
                Header = header,
                Body = body,
                Token = token
            };
        }
    }
}
