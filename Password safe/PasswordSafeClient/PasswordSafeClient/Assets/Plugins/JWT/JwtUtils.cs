﻿using Assets.Scripts.Application.JWT;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Plugins.JWT
{
    public static class JwtUtils
    {
        public static JwtModel ParseJwt(string token)
        {
            var splitInfo = token.Split('.');

            var headerStr = splitInfo[0].PadBase64StringToLength().DecodeBase64();

            var bodyStr = splitInfo[1].PadBase64StringToLength().DecodeBase64();

            var header = JsonConvert.DeserializeObject<JwtHeader>(headerStr);

            var body = JsonConvert.DeserializeObject<JwtBody>(bodyStr);

            return new JwtModel
            {
                Header = header,
                Body = body,
                Token = token
            };
        }
    }
}
