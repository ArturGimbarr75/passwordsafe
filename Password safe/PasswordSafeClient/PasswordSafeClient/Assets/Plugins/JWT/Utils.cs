﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Application.JWT
{
    public static class Utils
    {
        private static readonly DateTime Epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        public static string PadBase64StringToLength(this string s)
        {
            var curLength = s.Length;

            var padding = 4 - (curLength % 4);
            if (padding == 4)
                return s;
            var sb = new StringBuilder(curLength + padding);
            sb.Append(s);
            for (int i = 0; i < padding; i++)
            {
                sb.Append('=');
            }

            return sb.ToString();
        }

        public static string DecodeBase64(this string base64)
        {
            var bytes = Convert.FromBase64String(base64);
            return Encoding.UTF8.GetString(bytes);
        }

        public static DateTime FromUnixTimeStamp(long seconds)
        {
            return Epoch.AddSeconds(seconds);
        }
    }
}
