﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Application.JWT
{
    public class SecondsTimestampConverter : DateTimeConverterBase
    {

        private static readonly DateTime Epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var difference = (DateTime)value - Epoch;
            writer.WriteRawValue(((long)(difference).TotalSeconds).ToString());
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.Value == null) return null;

            var secondsSinceEpoch = serializer.Deserialize<long>(reader);
            return Epoch.AddSeconds(secondsSinceEpoch);
        }
    }
}
