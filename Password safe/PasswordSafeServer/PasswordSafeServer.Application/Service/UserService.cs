﻿using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using PasswordSafeServer.Application.CryptographyService;
using PasswordSafeServer.Application.DTO;
using PasswordSafeServer.Domain.Models;
using PasswordSafeServer.Persistance.Repositories;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace PasswordSafeServer.Application.Service
{
    public class UserService
    {
        private readonly IUserRepository _repository;
        private readonly IJWTRTHandler _handler;
        public UserService(IUserRepository repository, IJWTRTHandler handler)
        {
            _repository = repository;
            _handler = handler;
        }

        public async Task<ActionResult<TokensDTO>> RegisterAsync(AuthDTO auth)
        {
            try
            {
                if (!IsEmailValid(auth.Email))
                    return new StatusCodeResult((int)HttpStatusCode.BadRequest);
                if (!IsPasswordValid(auth.Password))
                    return new StatusCodeResult((int)HttpStatusCode.Unauthorized);

                var salt = SaltHandler.Create();
                var id = ObjectId.GenerateNewId();
                User user = new User()
                {
                    _id = id,
                    Email = auth.Email,
                    Password = HashHandler.Create(auth.Password, salt),
                    Salt = salt,
                    RefreshToken = _handler.CreateRefreshToken(id),
                    DataList = new DataListModel()
                };

                await _repository.InsertRecordAsync(user);

                return new TokensDTO()
                {
                    JWT = _handler.CreateJWT(user),
                    RefreshToken = user.RefreshToken
                };
            }
            catch
            {
                return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
            }
        }

        public async Task<ActionResult<TokensDTO>> UpdateTokens(AuthDTO auth, string refreshToken)
        {
            try
            {
                if (String.IsNullOrEmpty(auth.Email))
                    return new StatusCodeResult((int)HttpStatusCode.Unauthorized);

                var user = await _repository.LoadRecordByIdAsync(auth.Id);
                if (user.RefreshToken != refreshToken)
                    return new StatusCodeResult((int)HttpStatusCode.Unauthorized);

                user.RefreshToken = _handler.CreateRefreshToken(user._id);
                await _repository.EditRecordAsync(user);

                return new TokensDTO()
                {
                    JWT = _handler.CreateJWT(user),
                    RefreshToken = user.RefreshToken
                };
            }
            catch
            {
                return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
            }
        }

        public async Task<ActionResult> UpdateEmailAsync(AuthDTO auth)
        {
            try
            {
                if (!IsEmailValid(auth.Email))
                    return new StatusCodeResult((int)HttpStatusCode.BadRequest);

                var user = await _repository.LoadRecordByIdAsync(auth.Id);
                user.Email = auth.Email;

                await _repository.EditRecordAsync(user);

                return new StatusCodeResult((int)HttpStatusCode.OK);
            }
            catch
            {
                return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
            }
        }

        public async Task<ActionResult<TokensDTO>> LoginAsync(AuthDTO auth)
        {
            try
            {
                if (!IsEmailValid(auth.Email))
                    return new StatusCodeResult((int)HttpStatusCode.BadRequest);
                if (!IsPasswordValid(auth.Password))
                    return new StatusCodeResult((int)HttpStatusCode.Unauthorized);

                var user = await _repository.LoadRecordByEmailAsync(auth.Email);
                user.RefreshToken = _handler.CreateRefreshToken(user._id);
                await _repository.EditRecordAsync(user);

                return new TokensDTO()
                {
                    JWT = _handler.CreateJWT(user),
                    RefreshToken = user.RefreshToken
                };
            }
            catch
            {
                return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
            }
        }

        public async Task<ActionResult> UpdatePasswordAsync(AuthDTO auth)
        {
            try
            {
                var user = await _repository.LoadRecordByEmailAsync(auth.Email);
                if (user == null || !IsPasswordValid(auth.Password))
                    return new StatusCodeResult((int)HttpStatusCode.BadRequest);

                user.Salt = SaltHandler.Create();
                user.Password = HashHandler.Create(auth.Password, user.Salt);
                await _repository.EditRecordAsync(user);

                return new StatusCodeResult((int)HttpStatusCode.OK);
            }
            catch
            {
               return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
            }
        }

        public async Task<ActionResult> DeleteAsync(string id)
        {
            try
            {
                await _repository.DeleteRecordAsync(new ObjectId(id));
                return new StatusCodeResult((int)HttpStatusCode.OK);
            }
            catch
            {
                return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
            }
        }

        private static bool IsEmailValid(string email)
        {
            try
            {
                new MailAddress(email);
                return true;
            }
            catch
            {
                return false;
            }
        }

        private static bool IsPasswordValid(string password)
        {
            // Password must contain at least one digit
            // Password must contain at least one letter in low case
            // Password must contain at least one letter in high case
            // Password must contain at least six symbols
            // Password must contain at least one special character (!@#$%^&*)           
            Regex rule1 = new Regex("(?=.*[0-9])(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{6,}");
            // Password must contain only letters, digits and special characters
            Regex rule2 = new Regex("[^0-9a-zA-Z!@#$%^&*]");

            return rule1.IsMatch(password) && !rule2.IsMatch(password);
        }
    }
}
