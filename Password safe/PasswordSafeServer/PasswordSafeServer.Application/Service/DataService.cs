﻿using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using PasswordSafeServer.Application.DTO;
using PasswordSafeServer.Domain.Models;
using PasswordSafeServer.Persistance.Repositories;
using System.Net;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace PasswordSafeServer.Application.Service
{
    public class DataService
    {
        private readonly IDataModelRepository _repository;

        public DataService(IDataModelRepository repository)
        {
            _repository = repository;
        }

        public async Task<ActionResult> AddData(AuthDTO auth, DataModel data)
        {
            try
            {
                AddId(data);
                if (!IsDataModelValid(data) ||
                    string.IsNullOrEmpty(auth.Id.ToString()))
                    return new StatusCodeResult((int)HttpStatusCode.BadRequest);

                _repository.UserId = auth.Id;
                await _repository.InsertRecordAsync(data);
                return new StatusCodeResult((int)HttpStatusCode.OK);
            }
            catch
            {
                return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
            }
        }

        public async Task<ActionResult> ChangeData(AuthDTO auth, DataModel data)
        {
            try
            {
                if (!IsDataModelValid(data) ||
                    string.IsNullOrEmpty(auth.Id.ToString()))
                    return new StatusCodeResult((int)HttpStatusCode.BadRequest);

                _repository.UserId = auth.Id;
                await _repository.EditRecordAsync(data);

                return new StatusCodeResult((int)HttpStatusCode.OK);
            }
            catch
            {
                return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
            }
        }

        public async Task<ActionResult> DeleteDataElement(AuthDTO auth, DataModel data)
        {
            try
            {
                if (data?.ID == null ||
                    string.IsNullOrEmpty(auth.Id.ToString()))
                    return new StatusCodeResult((int)HttpStatusCode.BadRequest);

                _repository.UserId = auth.Id;
                await _repository.DeleteRecordAsync(data.ID);

                return new StatusCodeResult((int)HttpStatusCode.OK);
            }
            catch
            {
                return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
            }
        }

        public async Task<ActionResult> DeleteAllDataElements(AuthDTO auth)
        {
            try
            {
                if (string.IsNullOrEmpty(auth.Id.ToString()))
                    return new StatusCodeResult((int)HttpStatusCode.BadRequest);

                _repository.UserId = auth.Id;
                await _repository.DeleteRecordsByUserIdAsync();

                return new StatusCodeResult((int)HttpStatusCode.OK);
            }
            catch
            {
                return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
            }
        }

        public async Task<ActionResult<DataListDTO>> LoadAllData(AuthDTO auth)
        {
            try
            {
                if (string.IsNullOrEmpty(auth.Id.ToString()))
                    return new StatusCodeResult((int)HttpStatusCode.BadRequest);

                _repository.UserId = auth.Id;
                var result = await _repository.LoadRecordsAsync();

                return new DataListDTO() { DataModels = result.Select(x => new DataDTO()
                {
                    ID = x.ID.ToString(),
                    Email = x.Email,
                    Login = x.Login,
                    Password = x.Password,
                    WebsiteOrDevice = x.WebsiteOrDevice
                }).ToList() };
            }
            catch
            {
                return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
            }
        }

        public async Task<ActionResult<DataModel>> LoadData(AuthDTO auth, DataModel data)
        {
            try
            {
                if (string.IsNullOrEmpty(auth.Id.ToString()) ||
                    data?.ID == null)
                    return new StatusCodeResult((int)HttpStatusCode.BadRequest);

                _repository.UserId = auth.Id;
                var result = await _repository.LoadRecordByIdAsync(data.ID);

                return result;
            }
            catch
            {
                return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
            }
        }

        private bool IsDataModelValid(DataModel data)
        {
            return !string.IsNullOrEmpty(data.Password) &&
                !string.IsNullOrEmpty(data.WebsiteOrDevice) &&
                !string.IsNullOrEmpty(data?.ID.ToString());
        }

        private void AddId(DataModel data)
        {
            data.ID = ObjectId.GenerateNewId();
        }
    }
}
