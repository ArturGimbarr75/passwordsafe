﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;

namespace PasswordSafeServer.Application.DTO
{
    public class AuthDTO
    {
        public ObjectId Id { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
