﻿using PasswordSafeServer.Domain.Models;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Linq;
using System.Net;
using MongoDB.Bson;

namespace PasswordSafeServer.Application.CryptographyService
{
    public class JWTRTHandler : IJWTRTHandler
    {
        private readonly Microsoft.Extensions.Configuration.IConfiguration configuration;
        private JwtSecurityToken decodedToken;

        public JWTRTHandler(Microsoft.Extensions.Configuration.IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public string CreateJWT(User user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(configuration.GetSection("JWTToken:SecretKey").Value);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.NameIdentifier, user._id.ToString()),
                    new Claim(ClaimTypes.Email, user.Email),
                }),
                Expires = DateTime.UtcNow.AddSeconds(int.Parse(configuration.GetSection("JWTToken:JwtValidationTimeInSeconds").Value)),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)        
            };
            var tokenCreate = tokenHandler.CreateToken(tokenDescriptor);
            var token = tokenHandler.WriteToken(tokenCreate);
            return token;
        }

        public string CreateRefreshToken(ObjectId _id)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(configuration.GetSection("JWTToken:SecretKey").Value);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.NameIdentifier, _id.ToString()),
                }),
                Expires = DateTime.UtcNow.AddMinutes(int.Parse(configuration.GetSection("JWTToken:RTValidationTimeInSeconds").Value)),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var tokenCreate = tokenHandler.CreateToken(tokenDescriptor);
            var token = tokenHandler.WriteToken(tokenCreate);
            return token;
        }

        public bool IsValid(string token)
        {
            if (string.IsNullOrEmpty(token))
                return false;

            DecodeToken(token);
            var expiresAt = UnixTimeStampToDateTime((double)decodedToken.Payload.Exp);

            int result = DateTime.Compare(expiresAt, DateTime.UtcNow);
            return result > 0;
        }

        private static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Local);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }

        private void DecodeToken(string token)
        {
            var stream = token;
            var handler = new JwtSecurityTokenHandler();
            decodedToken = handler.ReadToken(stream) as JwtSecurityToken;
        }
    }
}
