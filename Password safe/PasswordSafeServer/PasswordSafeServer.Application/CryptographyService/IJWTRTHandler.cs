﻿using MongoDB.Bson;
using PasswordSafeServer.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace PasswordSafeServer.Application.CryptographyService
{
    public interface IJWTRTHandler
    {
        string CreateJWT(User user);
        string CreateRefreshToken(ObjectId _id);
        bool IsValid(string token);
    }
}
