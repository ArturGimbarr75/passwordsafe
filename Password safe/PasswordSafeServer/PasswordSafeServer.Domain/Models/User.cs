﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace PasswordSafeServer.Domain.Models
{
    public class User
    {
        public ObjectId _id { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Salt { get; set; }
        public string RefreshToken { get; set; }
        public DataListModel DataList { get; set; }
    }
}
