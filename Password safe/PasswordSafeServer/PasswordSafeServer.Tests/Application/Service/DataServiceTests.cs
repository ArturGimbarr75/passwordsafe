﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MongoDB.Bson;
using PasswordSafeServer.Application.DTO;
using PasswordSafeServer.Application.Service;
using PasswordSafeServer.Domain.Models;
using PasswordSafeServer.Persistance.Encoder;
using PasswordSafeServer.Persistance.Repositories;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace PasswordSafeServer.Tests.Application.Service
{
    public class DataServiceTests
    {
        User TestUser;
        DataService Service;
        UserRepository _UserRepository;
        AuthDTO Auth;
        Encryptor DataEncryptor;

        public DataServiceTests()
        {
            var curr = Environment.CurrentDirectory;
            var path = Path.Combine(curr, "..\\..\\..\\..", "PasswordSafeServer");
            var configuration = new ConfigurationBuilder()
            .SetBasePath(path)
            .AddJsonFile("appsettings.json")
            .Build();
            DataEncryptor = new Encryptor(configuration);
            Service = new DataService(new DataModelRepository(DataEncryptor));
            TestUser = new User()
            {
                _id = new ObjectId("11113207777702035cabcdef"),
                Email = "testuserdata@gmail.com",
                Password = "!Test12345",
                Salt = "X7400PDVtkDLQVA0TYV/nQ==",
                RefreshToken = String.Empty,
                DataList = new DataListModel()
                {
                    DataModels = new List<DataModel>()
                    {
                        Encode(new DataModel()
                        {
                            ID = new ObjectId("000000000000000000000001"),
                            WebsiteOrDevice = "www.facebook.com",
                            Email = "testuser@gmail.com",
                            Login = "TestUser",
                            Password = "testuser",
                        }),
                        Encode(new DataModel()
                        {
                            ID = new ObjectId("000000000000000000000002"),
                            WebsiteOrDevice = "www.youtube.com",
                            Email = "testuser@gmail.com",
                            Login = "TestUser",
                            Password = "testuser",
                        })
                    }
                }
            };
            Auth = new AuthDTO()
            {
                Id = TestUser._id,
                
            };
            _UserRepository = new UserRepository();
        }

        [Fact]
        public void Should_Add_Data()
        {
            RefreshUserForTests();
            var data = new DataModel()
            {
                ID = new ObjectId("0000000000000aa000000003"),
                WebsiteOrDevice = "www.vk.com",
                Email = "testuserdata@gmail.com",
                Login = "TestUser",
                Password = "testuser",
            };

            var result = Service.AddData(Auth, data).Result;

            Assert.Equal((int)HttpStatusCode.OK, ((StatusCodeResult)result).StatusCode);
        }

        [Fact]
        public void Should_Change_Data()
        {
            RefreshUserForTests();
            var data = new DataModel()
            {
                ID = TestUser.DataList.DataModels.FirstOrDefault().ID,
                WebsiteOrDevice = "www.vk.com",
                Email = "user@gmail.com",
                Login = "TesterUser",
                Password = "testeruser",
            };

            var result = Service.ChangeData(Auth, data).Result;

            Assert.Equal((int)HttpStatusCode.OK, ((StatusCodeResult)result).StatusCode);
        }

        [Fact]
        public void Should_Delete_All_Data()
        {
            RefreshUserForTests();

            var result = Service.DeleteAllDataElements(Auth).Result;

            Assert.Equal((int)HttpStatusCode.OK, ((StatusCodeResult)result).StatusCode);
        }

        [Fact]
        public void Should_Delete_Data()
        {
            RefreshUserForTests();
            var data = new DataModel()
            {
                ID = TestUser.DataList.DataModels.FirstOrDefault().ID,
            };

            var result = Service.DeleteDataElement(Auth, data).Result;

            Assert.Equal((int)HttpStatusCode.OK, ((StatusCodeResult)result).StatusCode);
        }

        [Fact]
        public void Should_Load_All_Data()
        {
            RefreshUserForTests();

            var result = Service.LoadAllData(Auth).Result;

            Assert.Equal(TestUser.DataList.DataModels.Count, result.Value.DataModels.Count);
        }

        [Fact]
        public void Should_Load_Data()
        {
            RefreshUserForTests();
            var data = new DataModel()
            {
                ID = TestUser.DataList.DataModels.FirstOrDefault().ID,
            };

            var result = Service.LoadData(Auth, data).Result;

            Assert.Equal(data.ID, result.Value.ID);
        }

        private void RefreshUserForTests()
        {
            try
            {
                var result = _UserRepository.LoadRecordByIdAsync(TestUser._id).Result;

                if (result != null)
                    _UserRepository.DeleteRecordAsync(TestUser._id);
            }
            finally
            {
                _UserRepository.InsertRecordAsync(TestUser);
            }
        }

        private DataModel Decode(DataModel data) => new DataModel()
        {
            Email = DataEncryptor.Decode(data.Email),
            Login = DataEncryptor.Decode(data.Login),
            Password = DataEncryptor.Decode(data.Password),
            WebsiteOrDevice = DataEncryptor.Decode(data.WebsiteOrDevice)
        };

        private DataModel Encode(DataModel data) => new DataModel()
        {
            ID = data.ID,
            Email = DataEncryptor.Encode(data.Email),
            Login = DataEncryptor.Encode(data.Login),
            Password = DataEncryptor.Encode(data.Password),
            WebsiteOrDevice = DataEncryptor.Encode(data.WebsiteOrDevice)
        };
    }
}
