﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MongoDB.Bson;
using PasswordSafeServer.Application.CryptographyService;
using PasswordSafeServer.Application.DTO;
using PasswordSafeServer.Application.Service;
using PasswordSafeServer.Domain.Models;
using PasswordSafeServer.Persistance.Encoder;
using PasswordSafeServer.Persistance.Repositories;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace PasswordSafeServer.Tests.Application.Service
{
    public class UserServiceTests
    {
        UserService Service;
        AuthDTO UserAuth;
        UserRepository _UserRepository;
        User TestUser;
        Encryptor DataEncryptor;

        public UserServiceTests()
        {
            var curr = Environment.CurrentDirectory;
            var path = Path.Combine(curr, "..\\..\\..\\..", "PasswordSafeServer");
            var configuration = new ConfigurationBuilder()
            .SetBasePath(path)
            .AddJsonFile("appsettings.json")
            .Build();
            DataEncryptor = new Encryptor(configuration);
            Service = new UserService(new UserRepository(), new JWTRTHandler(configuration));
            UserAuth = new AuthDTO()
            {
                Id = new ObjectId("111111177777012345abcdef"),
                Email = "testuser@gmail.com",
                Password = "!Test12345",
            };
            _UserRepository = new UserRepository();
            TestUser = new User()
            {
                _id = UserAuth.Id,
                Email = UserAuth.Email,
                Password = UserAuth.Password,
                Salt = "X7400PDVtkDLQVA0TYV",
                RefreshToken = "TestRefreshToken",
                DataList = new DataListModel()
                {
                    DataModels = new List<DataModel>()
                        {
                            Encode(new DataModel()
                            {
                                ID = new ObjectId("000000000000000000000001"),
                                WebsiteOrDevice = "www.facebook.com",
                                Email = "testuser@gmail.com",
                                Login = "TestUser",
                                Password = "testuser",
                            })
                        }
                }
            };
        }

        [Fact]
        public void Should_Register_User()
        {
            var auth = new AuthDTO()
            {
                Email = "registrationtest@gmail.com",
                Password = "!SuperPass123"
            };

            var result = Service.RegisterAsync(auth).Result;

            Assert.True(!String.IsNullOrEmpty(result.Value.JWT) && !String.IsNullOrEmpty(result.Value.RefreshToken));

            var id = _UserRepository.LoadRecordByEmailAsync(auth.Email).Result._id;
            _UserRepository.DeleteRecordAsync(id);
        }

        [Fact]
        public void Should_Delete_User()
        {
            RefreshUserForTests();

            var result = Service.DeleteAsync(UserAuth.Id.ToString()).Result;

            Assert.Equal((int)HttpStatusCode.OK, ((StatusCodeResult)result).StatusCode);
        }

        [Fact]
        public void Should_Login_User()
        {
            RefreshUserForTests();

            var result = Service.LoginAsync(UserAuth).Result;

            Assert.True(!String.IsNullOrEmpty(result.Value.JWT) && !String.IsNullOrEmpty(result.Value.RefreshToken));
        }

        [Fact]
        public void Should_Change_Email_User()
        {
            RefreshUserForTests();
            var auth = new AuthDTO()
            {
                Id = UserAuth.Id,
                Email = "newtestemail@gmail.com"
            };

            var result = Service.UpdateEmailAsync(auth).Result;

            Assert.Equal((int)HttpStatusCode.OK, ((StatusCodeResult)result).StatusCode);
        }

        [Fact]
        public void Should_Change_Password_User()
        {
            RefreshUserForTests();
            var auth = new AuthDTO()
            {
                Id = UserAuth.Id,
                Email = UserAuth.Email,
                Password = "!newPassword123"
            };

            var result = Service.UpdatePasswordAsync(auth).Result;

            Assert.Equal((int)HttpStatusCode.OK, ((StatusCodeResult)result).StatusCode);
        }

        [Fact]
        public void Should_Change_Tokens_User()
        {
            RefreshUserForTests();
            var newToken = "NewSuperDuperToken";

            var result = Service.UpdateTokens(UserAuth, TestUser.RefreshToken).Result;

            Assert.True(!String.IsNullOrEmpty(result.Value.JWT) && !String.IsNullOrEmpty(result.Value.RefreshToken));
        }

        private void RefreshUserForTests()
        {
            try
            {
                var result = _UserRepository.LoadRecordByIdAsync(UserAuth.Id).Result;

                if (result != null)
                    _UserRepository.DeleteRecordAsync(UserAuth.Id);
            }
            finally
            {
                _UserRepository.InsertRecordAsync(TestUser);
            }
        }

        private DataModel Decode(DataModel data) => new DataModel()
        {
            Email = DataEncryptor.Decode(data.Email),
            Login = DataEncryptor.Decode(data.Login),
            Password = DataEncryptor.Decode(data.Password),
            WebsiteOrDevice = DataEncryptor.Decode(data.WebsiteOrDevice)
        };

        private DataModel Encode(DataModel data) => new DataModel()
        {
            ID = data.ID,
            Email = DataEncryptor.Encode(data.Email),
            Login = DataEncryptor.Encode(data.Login),
            Password = DataEncryptor.Encode(data.Password),
            WebsiteOrDevice = DataEncryptor.Encode(data.WebsiteOrDevice)
        };
    }
}
