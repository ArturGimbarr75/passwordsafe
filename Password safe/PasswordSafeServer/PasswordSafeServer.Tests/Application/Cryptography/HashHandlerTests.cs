using PasswordSafeServer.Application.CryptographyService;
using System;
using Xunit;

namespace PasswordSafeServer.Tests.Application.Cryptography
{
    public class HashHandlerTests
    {
        [Fact]
        public void Passwords_Hashes_Should_Match_Regretion()
        {
            const string pass = "SuperPassword123456789";
            string salt = "X7400PDVtkDLQVA0TYV/nQ==";
            const string expected = "b5OJKzb6gonC3r2Fq5vJnqACO4f9ZGCUizLmUHYRJKQ=";

            string result = HashHandler.Create(pass, salt);

            Assert.Equal(expected, result);
        }
    }
}
