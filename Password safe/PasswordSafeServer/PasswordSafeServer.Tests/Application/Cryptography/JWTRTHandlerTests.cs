﻿using Microsoft.Extensions.Configuration;
using MongoDB.Bson;
using MongoDB.Driver;
using PasswordSafeServer.Application.CryptographyService;
using PasswordSafeServer.Domain.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Text;
using Xunit;

namespace PasswordSafeServer.Tests.Application.Cryptography
{
    public class JWTRTHandlerTests
    {
        [Fact]
        public void Should_Put_Email_Into_JWT()
        {
            var id = new ObjectId("1f113207777702035c265a83");
            var email = "artur@gmail.com";
            User user = new User()
            {
                _id = id,
                Email = email
            };
            var curr = Environment.CurrentDirectory;
            var path = Path.Combine(curr, "..\\..\\..\\..", "PasswordSafeServer");
            IConfiguration configuration = new ConfigurationBuilder()
            .SetBasePath(path)
            .AddJsonFile("appsettings.json")
            .Build();

            var jwtHandler = new JWTRTHandler(configuration);
            var jwt = jwtHandler.CreateJWT(user);
            var result = (new JwtSecurityTokenHandler().ReadToken(jwt) as JwtSecurityToken).Claims.First(claim => claim.Type == "email").Value;

            Assert.Equal(email, result);
        }

        [Fact]
        public void Should_Put_Id_Into_JWT()
        {
            var id = new ObjectId("1f113207777702035c265a83");
            var email = "artur@gmail.com";
            User user = new User()
            {
                _id = id,
                Email = email
            };
            var curr = Environment.CurrentDirectory;
            var path = Path.Combine(curr, "..\\..\\..\\..", "PasswordSafeServer");
            IConfiguration configuration = new ConfigurationBuilder()
            .SetBasePath(path)
            .AddJsonFile("appsettings.json")
            .Build();

            var jwtHandler = new JWTRTHandler(configuration);
            var jwt = jwtHandler.CreateJWT(user);
            var result = (new JwtSecurityTokenHandler().ReadToken(jwt) as JwtSecurityToken).Claims.First(claim => claim.Type == "nameid").Value;

            Assert.Equal(id.ToString(), result);
        }

        [Fact]
        public void Should_Correct_Put_EXP()
        {
            var id = new ObjectId("1f113207777702035c265a83");
            var email = "artur@gmail.com";
            User user = new User()
            {
                _id = id,
                Email = email
            };
            var curr = Environment.CurrentDirectory;
            var path = Path.Combine(curr, "..\\..\\..\\..", "PasswordSafeServer");
            IConfiguration configuration = new ConfigurationBuilder()
            .SetBasePath(path)
            .AddJsonFile("appsettings.json")
            .Build();

            var jwtHandler = new JWTRTHandler(configuration);
            var jwt = jwtHandler.CreateJWT(user);
            var iat = long.Parse((new JwtSecurityTokenHandler().ReadToken(jwt) as JwtSecurityToken).Claims.First(claim => claim.Type == "iat").Value);
            var exp = long.Parse((new JwtSecurityTokenHandler().ReadToken(jwt) as JwtSecurityToken).Claims.First(claim => claim.Type == "exp").Value);
            var validationTime = long.Parse(configuration.GetSection("JWTToken:JWTValidationTimeInSeconds").Value);

            Assert.Equal(validationTime, exp - iat);
        }
    }
}
