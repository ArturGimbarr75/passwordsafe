﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using PasswordSafeServer.Application.DTO;
using PasswordSafeServer.Application.Service;
using PasswordSafeServer.Domain.Models;

namespace PasswordSafeServer.API.Controllers
{
    [ApiController]
    public class DataController : Controller
    {
        private UserService _userService;
        private DataService _dataService;

        public DataController(UserService userService, DataService dataService)
        {
            _userService = userService;
            _dataService = dataService;
        }

        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [Route("/api/data/add")]
        public async Task<ActionResult> Add([FromBody] DataDTO data)
        {
            return await _dataService.AddData(ConvertToAuthDTO(User), ConvertToDataModel(data));
        }

        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [Route("/api/data/change_data")]
        public async Task<ActionResult> ChangeData([FromBody] DataDTO data)
        {
            return await _dataService.ChangeData(ConvertToAuthDTO(User), ConvertToDataModel(data));
        }

        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [Route("/api/data/delete_element")]
        public async Task<ActionResult> DeleteData([FromBody] DataDTO data)
        {
            return await _dataService.DeleteDataElement(ConvertToAuthDTO(User), ConvertToDataModel(data));
        }

        [HttpDelete]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [Route("/api/data/delete_all_data")]
        public async Task<ActionResult> DeleteAllData()
        {
            return await _dataService.DeleteAllDataElements(ConvertToAuthDTO(User));
        }

        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [Route("/api/data/get_all_data")]
        public async Task<ActionResult<DataListDTO>> GetAllData()
        {
            return await _dataService.LoadAllData(ConvertToAuthDTO(User));
        }

        private AuthDTO ConvertToAuthDTO(ClaimsPrincipal user)
        {
            return new AuthDTO()
            {
                Id = new MongoDB.Bson.ObjectId(
                user.Claims.Where(x => x.Type == ClaimTypes.NameIdentifier).Select(x => x.Value).FirstOrDefault())
            };
        }

        private DataModel ConvertToDataModel(DataDTO data)
        {
            try
            {
                return new Domain.Models.DataModel()
                {
                    ID = new ObjectId(data.ID),
                    Email = data.Email,
                    Login = data.Login,
                    Password = data.Password,
                    WebsiteOrDevice = data.WebsiteOrDevice
                };
            }
            catch
            {
                return null;
            }
        }
    }
}
