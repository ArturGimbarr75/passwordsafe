﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using PasswordSafeServer.Application.DTO;
using PasswordSafeServer.Application.Service;
using PasswordSafeServer.Domain.Models;
using PasswordSafeServer.Persistance.Repositories;

namespace PasswordSafeServer.API.Controllers
{
    [ApiController]
    public class AuthController : Controller
    {
        private UserService _userService;
        private DataService _dataService;

        public AuthController(UserService userService, DataService dataService)
        {
            _userService = userService;
            _dataService = dataService;
        }

        [HttpPost]
        [AllowAnonymous]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [Route("/api/auth/register")]
        public async Task<ActionResult<TokensDTO>> Registration([FromBody] AuthDTO auth)
        {
            return await _userService.RegisterAsync(auth);
        }

        [HttpPost]
        [AllowAnonymous]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [Route("/api/auth/login")]
        public async Task<ActionResult<TokensDTO>> Login([FromBody] AuthDTO auth)
        {
            return await _userService.LoginAsync(auth);
        }

        [HttpDelete]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [Route("/api/auth/delete")]
        public async Task<ActionResult> Delete()
        {
            return await _userService.DeleteAsync(User.Claims.Where(x => x.Type == ClaimTypes.NameIdentifier).Select(x => x.Value).FirstOrDefault());
        }

        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [Route("/api/auth/change_password")]
        public async Task<ActionResult> ChangePassword([FromBody] AuthDTO auth)
        {
            return await _userService.UpdatePasswordAsync(new AuthDTO() { Email = User.FindFirst(ClaimTypes.Email).Value, Password = auth.Password });
        }

        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [Route("/api/auth/change_email")]
        public async Task<ActionResult> ChangeEmail([FromBody] AuthDTO auth)
        {
            return await _userService.UpdateEmailAsync(new AuthDTO() { Id = new ObjectId(User.FindFirst(ClaimTypes.NameIdentifier).Value), Email = auth.Email });
        }

        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [Route("/api/auth/refresh")]
        public async Task<ActionResult<TokensDTO>> GetNewTokens([FromBody] TokensDTO tokens)
        {
            return await _userService.UpdateTokens(new AuthDTO() { Id = new ObjectId(User.FindFirst(ClaimTypes.NameIdentifier)?.Value),
                Email = User.FindFirst(ClaimTypes.Email)?.Value},
                tokens.RefreshToken);
        }

    }
}
