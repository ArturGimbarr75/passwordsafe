﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PasswordSafeServer.Persistance.Models
{
    public class Settings
    {
        public string ConnectionString { get; set; }
        public string Database { get; set; }
        public string Table { get; set; }
    }
}
