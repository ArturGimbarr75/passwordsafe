﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PasswordSafeServer.Persistance.Encoder.Cipher
{
    interface ICipherType
    {
        string Encode(string line);

        string Decode(string line);
    }
}
