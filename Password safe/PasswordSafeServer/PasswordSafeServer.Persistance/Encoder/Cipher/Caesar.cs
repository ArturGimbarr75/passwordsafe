﻿
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using PasswordSafeServer.Persistance.Encoder.StringExtension;
using System.Text.RegularExpressions;

namespace PasswordSafeServer.Persistance.Encoder.Cipher
{
    class Caesar : ICipherType
    {
        private int Shift;
        public const int ID = 1;
        public Caesar(IConfiguration configuration)
        {
            Shift = int.Parse(configuration.GetSection("Caesar:Shift").Value);
        }

        public string Decode(string line) => Coding(line, true);

        public string Encode(string line) => Coding(line, false);

        private string Coding(string line, bool reverse)
        {
            for (int i = 0; i < line.Length; i++)
            {
                Regex regex = new Regex("[^A-Za-z0-9]");
                if (regex.IsMatch(line[i].ToString()))
                    continue;
                int ch = line[i];
                int aligment = (ch >= '0' && ch <= '9') ? '9' - '0' + 1 : 'Z' - 'A' + 1;
                ch += ((reverse) ? -Shift : Shift);

                ch = line[i].AlignChar(ch, aligment);

                line = line.ReplaceCharInString(i, (char)ch);
            }
            return line;
        }
    }
}
