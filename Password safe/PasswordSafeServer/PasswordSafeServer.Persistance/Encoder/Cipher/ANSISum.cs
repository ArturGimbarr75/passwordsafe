﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using PasswordSafeServer.Persistance.Encoder.StringExtension;
using System.Text.RegularExpressions;

namespace PasswordSafeServer.Persistance.Encoder.Cipher
{
    class ANSISum : ICipherType
    {
        public const int ID = 2;
        public string Decode(string line)
        {
            Regex regex = new Regex("[^A-Za-z0-9]");
            for (int i = line.Length - 1; i > 0; i--)
            {
                if (regex.IsMatch(line[i].ToString()))
                    continue;
                int ch = line[i] - ((int)line[i - 1]).ToString().Sum(x => int.Parse(x.ToString()));
                int aligment = (line[i] >= '0' && line[i] <= '9') ? '9' - '0' + 1 : 'Z' - 'A' + 1;

                ch = line[i].AlignChar(ch, aligment);

                line = line.ReplaceCharInString(i, (char)ch);
            }

            return line.Substring(1, line.Length-1);
        }

        public string Encode(string line)
        {
            Random rand = new Random();
            line = ((char)rand.Next('a', 'z')).ToString() + line;
            Regex regex = new Regex("[^A-Za-z0-9]");

            for (int i = 1; i < line.Length; i++)
            {
                if (regex.IsMatch(line[i].ToString()))
                    continue;
                int ch = line[i] + ((int)line[i - 1]).ToString().Sum(x => int.Parse(x.ToString()));
                int aligment = (line[i] >= '0' && line[i] <= '9') ? '9' - '0' + 1 : 'Z' - 'A' + 1;

                ch = line[i].AlignChar(ch, aligment);

                line = line.ReplaceCharInString(i, (char)ch);
            }

            return line;
        }
    }
}
