﻿using Microsoft.Extensions.Configuration;
using PasswordSafeServer.Persistance.Encoder.StringExtension;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace PasswordSafeServer.Persistance.Encoder.Cipher
{
    class DictionaryCipher : ICipherType
    {
        private Dictionary<char, char> SymbolDict;
        private int Seed;
        public const int ID = 3;
        public DictionaryCipher(IConfiguration configuration)
        {
            Seed = int.Parse(configuration.GetSection("DictionaryCipher:Seed").Value);
            InitialiseDictionary();
        }

        public string Decode(string line)
        {
            var encodeDict = SymbolDict.ToDictionary(x => x.Value, x => x.Key);
            for (int i = 0; i < line.Length; i++)
                if (encodeDict.ContainsKey(line[i]))
                    line = line.ReplaceCharInString(i, encodeDict[line[i]]);

            return line;
        }

        public string Encode(string line)
        {
            for (int i = 0; i < line.Length; i++)
                if (SymbolDict.ContainsKey(line[i]))
                    line = line.ReplaceCharInString(i, SymbolDict[line[i]]);

            return line;
        }

        private void InitialiseDictionary()
        {
            SymbolDict = new Dictionary<char, char>();
            List<char> symbs = new List<char>();

            for (char i = '0'; i <= '9'; i++)
                symbs.Add(i);
            for (char i = 'a'; i <= 'z'; i++)
                symbs.Add(i);
            for (char i = 'A'; i <= 'Z'; i++)
                symbs.Add(i);

            Random rand = new Random(Seed);

            var symbsKey = new List<char>(symbs);
            symbs = symbs.OrderBy(x => rand.Next()).ToList();

            for (int i = 0; i < symbsKey.Count; i++)
                SymbolDict.Add(symbsKey[i], symbs[i]);
        }
    }
}
