﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using PasswordSafeServer.Persistance.Encoder.Cipher;

namespace PasswordSafeServer.Persistance.Encoder
{
    public class Encryptor : IEncryptor
    {
        private List<ICipherType> EncryptorsCiphers;
        private IConfiguration Configuration;

        public Encryptor(IConfiguration configuration)
        {
            Configuration = configuration;
            this.EncryptorsCiphers = GetCiphers(configuration.GetSection("Ciphers").Value);
        }

        private List<ICipherType> GetCiphers(string ciphers)
        {
            List<ICipherType> list = new List<ICipherType>();

            foreach (char ch in ciphers)
            {
                switch (int.Parse(ch.ToString()))
                {
                    case ANSISum.ID:
                        list.Add(new ANSISum());
                        break;

                    case Caesar.ID:
                        list.Add(new Caesar(Configuration));
                        break;

                    case DictionaryCipher.ID:
                        list.Add(new DictionaryCipher(Configuration));
                        break;
                }
            }

            return list;
        }

        public string Encode(string line)
        {
            string result = line;
            foreach (var cipher in EncryptorsCiphers)
            {
                result = cipher.Encode(result);
            }
            return result;
        }

        public string Decode(string line)
        {
            string result = line;
            for (int i = EncryptorsCiphers.Count - 1; i >= 0; i--)
            {
                result = EncryptorsCiphers[i].Decode(result);
            }
            return result;
        }
    }
}
