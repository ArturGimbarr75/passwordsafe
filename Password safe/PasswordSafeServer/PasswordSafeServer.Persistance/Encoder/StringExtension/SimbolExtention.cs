﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PasswordSafeServer.Persistance.Encoder.StringExtension
{
    static class SimbolExtention
    {
        public static string ReplaceCharInString(this String str, int index, Char newSymb)
            => str.Remove(index, 1).Insert(index, newSymb.ToString());

        public static char AlignChar(this char originalValue, int value, int aligment)
        {
            char min, max;
            if (originalValue >= '0' && originalValue <= '9')
            {
                min = '0';
                max = '9';
            }
            else if (originalValue >= 'A' && originalValue <= 'Z')
            {
                min = 'A';
                max = 'Z';
            }
            else
            {
                min = 'a';
                max = 'z';
            }


            if (originalValue >= min && originalValue <= max)
                while (value < min || value > max)
                    if (value < min)
                        value += aligment;
                    else
                        value -= aligment;

            return (char)value;
        }
    }
}
