﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PasswordSafeServer.Persistance.Encoder
{
    public interface IEncryptor
    {
        string Encode(string line);
        string Decode(string line);
    }
}
