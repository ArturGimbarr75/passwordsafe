﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;
using PasswordSafeServer.Persistance.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace PasswordSafeServer.Persistance.Data
{
    class ApplicationDbContext<T> : IApplicationDbContext<T> where T : class
    {
        private readonly IMongoDatabase _database;
        private readonly string _table;
        public ApplicationDbContext(IOptions<Settings> settings)
        {
            var client = new MongoClient();
            _table = settings.Value.Table;
            if (client != null)
                _database = client.GetDatabase(settings.Value.Database);
        }

        public IMongoCollection<T> Collection
        {
            get
            {
                return _database.GetCollection<T>(_table);
            }
        }
    }
}
