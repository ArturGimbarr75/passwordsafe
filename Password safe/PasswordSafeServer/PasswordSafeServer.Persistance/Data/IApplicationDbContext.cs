﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;

namespace PasswordSafeServer.Persistance.Data
{
    public interface IApplicationDbContext<T> where T : class
    {
        IMongoCollection<T> Collection { get; }
    } 
}
