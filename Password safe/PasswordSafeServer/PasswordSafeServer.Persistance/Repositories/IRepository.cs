﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PasswordSafeServer.Persistance.Repositories
{
    public interface IRepository<T> where T : class
    {
        Task DeleteRecordAsync(ObjectId id);
        Task InsertRecordAsync(T record);
        Task<T> LoadRecordByIdAsync(ObjectId id);
        Task<List<T>> LoadRecordsAsync();
        Task EditRecordAsync(T record);
    }
}
