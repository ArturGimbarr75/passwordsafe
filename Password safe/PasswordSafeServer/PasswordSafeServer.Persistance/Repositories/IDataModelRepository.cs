﻿using MongoDB.Bson;
using PasswordSafeServer.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PasswordSafeServer.Persistance.Repositories
{
    public interface IDataModelRepository : IRepository<DataModel>
    {
        ObjectId UserId { set; }
        Task DeleteRecordsByUserIdAsync();
    }
}
