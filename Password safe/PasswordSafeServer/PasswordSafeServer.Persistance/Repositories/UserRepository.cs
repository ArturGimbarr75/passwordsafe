﻿using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using PasswordSafeServer.Domain.Models;
using PasswordSafeServer.Persistance.Data;
using PasswordSafeServer.Persistance.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PasswordSafeServer.Persistance.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly ApplicationDbContext<User> context;
        public UserRepository()
        {
            context = new ApplicationDbContext<User>
                (
                    Options.Create(
                    new Settings
                    {
                        Database = "PasswordSafe",
                        Table = "Users"
                    })
                );
        }

        public async Task DeleteRecordAsync(ObjectId id)
        {
            var filter = Builders<User>.Filter.Eq("_id", id);
            await context.Collection.DeleteOneAsync(filter);
        }

        public async Task InsertRecordAsync(User record)
        {
            context
                .Collection
                .Indexes
                .CreateOne(
                    new CreateIndexModel<User>(
                        new IndexKeysDefinitionBuilder<User>().Ascending(x => x.Email).Ascending(x => x._id),
                        new CreateIndexOptions { Unique = true }));
            await context.Collection.InsertOneAsync(record);
        }

        public async Task<User> LoadRecordByEmailAsync(string email)
        {
            var filter = Builders<User>.Filter.Eq("Email", email);
            return await context.Collection.Find(filter).FirstAsync();
        }

        public async Task<User> LoadRecordByIdAsync(ObjectId id)
        {
            var filter = Builders<User>.Filter.Eq("_id", id);
            return await context.Collection.Find(filter).FirstAsync();
        }

        public async Task<List<User>> LoadRecordsAsync()
        {
            return await context.Collection.Find(new BsonDocument()).ToListAsync();
        }

        public async Task EditRecordAsync(User record)
        {
            await context.Collection.ReplaceOneAsync(
                 new BsonDocument("_id", record._id),
                 record,
                 new ReplaceOptions { IsUpsert = true });
        }
    }
}
