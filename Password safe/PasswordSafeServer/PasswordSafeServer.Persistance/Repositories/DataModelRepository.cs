﻿using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using PasswordSafeServer.Domain.Models;
using PasswordSafeServer.Persistance.Data;
using PasswordSafeServer.Persistance.Encoder;
using PasswordSafeServer.Persistance.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PasswordSafeServer.Persistance.Repositories
{
    public class DataModelRepository : IDataModelRepository
    {

        private readonly ApplicationDbContext<User> context;
        public ObjectId UserId { private get; set; }
        private IEncryptor DataEncryptor;
        public DataModelRepository(IEncryptor encryptor)
        {
            DataEncryptor = encryptor;
            context = new ApplicationDbContext<User>
                (
                    Options.Create(
                    new Settings
                    {
                        Database = "PasswordSafe",
                        Table = "Users"
                    })
                );
        }

        public async Task DeleteRecordAsync(ObjectId id)
        {
            var filter = Builders<User>.Filter.Eq("_id", UserId);
            var update = Builders<User>.Update.PullFilter(x => x.DataList.DataModels,
                Builders<DataModel>.Filter.Eq(x => x.ID, id));

            await context.Collection.UpdateOneAsync(filter, update);
        }

        public async Task DeleteRecordsByUserIdAsync()
        {
            var filter = Builders<User>.Filter.Eq("_id", UserId);
            var update = Builders<User>.Update.Set("DataList.DataModels", new List<DataModel>());
            await context.Collection.UpdateOneAsync(filter, update);
        }

        public async Task EditRecordAsync(DataModel record)
        {
            var filter = Builders<User>.Filter.Eq("_id", UserId) &
                Builders<User>.Filter.ElemMatch(x => x.DataList.DataModels,
                    Builders<DataModel>.Filter.Eq(x => x.ID, record.ID));
            var update = Builders<User>.Update.Set(x => x.DataList.DataModels[-1], Encode(record));
            await context.Collection.UpdateOneAsync(filter, update);
        }

        public async Task InsertRecordAsync(DataModel record)
        {
            var filter = Builders<User>.Filter.Eq("_id", UserId);
            var update = Builders<User>.Update.Push("DataList.DataModels", Encode(record));
            await context.Collection.UpdateOneAsync(filter, update);
        }

        public async Task<DataModel> LoadRecordByIdAsync(ObjectId id)
        {
            var filter = Builders<User>.Filter.Eq("_id", UserId);
            var result = (await context.Collection.Find(filter).FirstAsync())
                .DataList.DataModels.Select(x => x).Where(x => x.ID == id).FirstOrDefault();
            return Decode(result);
        }

        public async Task<List<DataModel>> LoadRecordsAsync()
        {
            var filter = Builders<User>.Filter.Eq("_id", UserId);
            return (await context.Collection.Find(filter).FirstAsync()).DataList.DataModels.Select(x => Decode(x)).ToList();
        }

        private DataModel Decode(DataModel data) => new DataModel()
        {
            ID = data.ID,
            Email = DataEncryptor.Decode(data.Email),
            Login = DataEncryptor.Decode(data.Login),
            Password = DataEncryptor.Decode(data.Password),
            WebsiteOrDevice = DataEncryptor.Decode(data.WebsiteOrDevice)
        };

        private DataModel Encode(DataModel data) => new DataModel()
        {
            ID = data.ID,
            Email = DataEncryptor.Encode(data.Email),
            Login = DataEncryptor.Encode(data.Login),
            Password = DataEncryptor.Encode(data.Password),
            WebsiteOrDevice = DataEncryptor.Encode(data.WebsiteOrDevice)
        };
    }
}
