﻿using PasswordSafeServer.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PasswordSafeServer.Persistance.Repositories
{
    public interface IUserRepository : IRepository<User>
    {
        Task<User> LoadRecordByEmailAsync(string email);
    }
}
