# Password safe server

Project - a server that stores data to connect users to different sites or devices

## The work the server does

1) Stores data

2) Allows the user to perform CRUD operations

3) Encrypts user data

# appsettings.json

application settings

## JWTToken

1) SecretKey - jwt secret key

2) JwtValidationTimeInSeconds - Time during which the token is valid

3) RTValidationTimeInSeconds - Time during which the refresh token is valid

## Ciphers

A queue of encryptors ids. Data will be encrypted in this order

## Caesar

1) Shift -Character offset value

## DictionaryCipher

1) Seed - Seed for random shuffling of characters